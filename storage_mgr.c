#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "storage_mgr.h"


/* manipulating page files */
void initStorageManager (void)
{
	//leave empty
}

RC createPageFile (char *fileName)
{
	//handle error of file_not_found
	if (fileName == NULL)
		return RC_FILE_NOT_FOUND;
	//handle error of write_failed
	FILE* pF;
	if((pF=fopen(fileName,"wb"))==NULL)
		return RC_WRITE_FAILED;
	else
	{
		//initialize the file with one empty page
		int pageC=1;
		fwrite(&pageC,sizeof(int),1,pF);
		//malloc a PAGE_SIZE bytes memory, set to zero for each bytes
		char* emptyPage=(char*)calloc(PAGE_SIZE,sizeof(char));
		//set the file position indicator sizeof(int) bytes from the beginning
		fseek(pF,sizeof(int),SEEK_SET);
		//write the emptypage zero block from memory into file
		fwrite(emptyPage,sizeof(char),PAGE_SIZE,pF);
		//always remember to close the file
		fclose(pF);
		pF=NULL;
		free(emptyPage);
		emptyPage=NULL;
		return RC_OK;
	}
}

RC openPageFile (char *fileName, SM_FileHandle *fHandle)
{
	//handle error of file_not_found
	if (fileName == NULL) {
		return RC_FILE_NOT_FOUND;
	}
	//handle error of file_not_init
	if (fHandle == NULL) {
		return RC_FILE_HANDLE_NOT_INIT;
	}
	FILE* pF;
	if((pF=fopen(fileName,"rb+"))==NULL)
	{
		return RC_FILE_NOT_FOUND;
	}
	else
	{
		//seek to the beginning of the file, the page number is stored at the first sizeof(int) bytes 
		fseek(pF,0,SEEK_SET);
		int pageC;
		fread(&pageC,sizeof(int),1,pF);
		//fill in the fHandle, set current page position to be zero
		//mgmtInfo is used for storing FILE* pointer.
		fHandle->fileName=fileName;
		fHandle->totalNumPages=pageC;
		fHandle->curPagePos=0;
		fHandle->mgmtInfo=pF;
		return RC_OK;
	}
}

RC closePageFile (SM_FileHandle *fHandle)
{
	//handle error of file_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	if(fclose(fHandle->mgmtInfo)==0)
	{
		fHandle->fileName=NULL;
		fHandle->totalNumPages=0;
		fHandle->curPagePos=0;
		fHandle->mgmtInfo=NULL;
		return RC_OK;
	}
	else return RC_FILE_HANDLE_NOT_INIT;
}

RC destroyPageFile (char *fileName)
{
	if(remove(fileName)==0)
		return RC_OK;
	else
		return RC_FILE_NOT_FOUND;
}

/* reading blocks from disc */
RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	if (memPage == NULL)
		return RC_READ_NON_EXISTING_PAGE;
	if (pageNum >= fHandle->totalNumPages)
		return RC_READ_NON_EXISTING_PAGE;
	if (pageNum < 0)
		return RC_READ_NON_EXISTING_PAGE;
	if (fHandle->mgmtInfo == NULL)
		return RC_FILE_NOT_FOUND;
	FILE* pF=(FILE *)fHandle->mgmtInfo;
	//offset the block stored total number of pages
	int offset=1*sizeof(int);
	//seek to the beginning of the required block for reading, pageNum is the curPagePos
	if(fseek(pF,(pageNum)*PAGE_SIZE+offset,SEEK_SET)==0)
	{
		fread(memPage,sizeof(char),PAGE_SIZE,pF);
		fHandle->curPagePos = pageNum;
		return RC_OK;
	}
	else return RC_READ_NON_EXISTING_PAGE;
}

int getBlockPos (SM_FileHandle *fHandle)
{
	//handle error of file_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return fHandle->curPagePos;
}

RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	//handle error of file_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return readBlock(0,fHandle,memPage);
}

RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	//handle error of File_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return readBlock(fHandle->curPagePos-1,fHandle,memPage);
}

RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	//handle error of File_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return readBlock(fHandle->curPagePos,fHandle,memPage);
}

RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	//handle error of File_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return readBlock(fHandle->curPagePos+1,fHandle,memPage);
}

RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	//handle error of File_handle_not_init
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	return readBlock(fHandle->totalNumPages,fHandle,memPage);
}

/* writing blocks to a page file */
RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	if (memPage == NULL)
		return RC_READ_NON_EXISTING_PAGE;
	if (fHandle->mgmtInfo == NULL)
		return RC_FILE_NOT_FOUND;
	//pageNum is current page position
	if (pageNum >= fHandle->totalNumPages)
		if (ensureCapacity(pageNum+1, fHandle) != RC_OK)
			return RC_WRITE_FAILED;
	FILE* pF=(FILE *)fHandle->mgmtInfo;
	int offset=1*sizeof(int);//offset the block stored total number of pages
	if(fseek(pF,(pageNum)*PAGE_SIZE+offset,SEEK_SET)==0)
	{
		int i;
		fwrite(memPage,sizeof(char),PAGE_SIZE,pF);
		//since we write a page more, curPagePos adds up 1
		fHandle->curPagePos = pageNum + 1;
		return RC_OK;
	}
	else return RC_WRITE_FAILED;
}

RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	if (fHandle == NULL)
		return RC_READ_NON_EXISTING_PAGE;
	return writeBlock(fHandle->curPagePos,fHandle, memPage);
}

RC appendEmptyBlock (SM_FileHandle *fHandle)
{
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	if (fHandle->mgmtInfo == NULL)
		return RC_FILE_NOT_FOUND;
	//malloc an empty block in memory to write to disk
	char * memPage=(char*)calloc(PAGE_SIZE,sizeof(char));
	//get the FILE pointer
	FILE *pF = (FILE *)fHandle->mgmtInfo;
	//seek to the end of the file and start writing empty block
	fseek(pF, 0, SEEK_END);
	fwrite(memPage,sizeof(char),PAGE_SIZE,pF);
	//add up one to total number of pages
	fHandle->totalNumPages++;
	//seek to the beginning of the file to change the total number of file
	fseek(pF,0,SEEK_SET);
	int numOfPages = fHandle->totalNumPages;
	fwrite(&numOfPages, sizeof(int), 1, pF);
	free(memPage);
	memPage=NULL;
	return RC_OK;
}

RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle)
{
	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;
	//if total number of pages is less than required, we append empty block at the end.
	while(fHandle->totalNumPages < numberOfPages)
	{
		if (appendEmptyBlock(fHandle) != RC_OK)
			return RC_WRITE_FAILED;
	}
	return RC_OK;
}


