#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"
#include "rm_serializer.c"
#include "buffer_mgr.c"

#define TABLE_NAME 30
#define NUM_TUPLES 5
#define NUM_ATTRS 5
#define NUM_KEYS 5
#define ATTR_TYPE_LENGTH 5
#define ATTR_NAME 20
#define KEY_POSITION 2

//store management info of a table
typedef struct TableMgr
{
	int numTuple;
	int schemaSize;
	BM_BufferPool* bm;
	BM_PageHandle *h;

}TableMgr;


typedef struct ScanMgr
{
	int page;//store the current position of the scan handler
	int slot;
	Expr* cond;
}ScanMgr;

//serialize functions
char* mySerializeSchema(Schema* schema);
char* mySerializeTableInfo(RM_TableData* rel);
void myDeSerializeSchema(Schema* schema, char* info);
void myDeSerializeTableInfo(RM_TableData* rel, char* info);
int getSchemaSize(int numAttr, int keySize);
int getPageHeaderSize(RM_TableData* rel);

int getPageHeaderSize(RM_TableData* rel)
{
	int recordSize=getRecordSize(rel->schema);
	return PAGE_SIZE/(recordSize+1);//it is equal to number of records in one page
}

RC freeSchema(Schema* schema)
{
	int numAttrs=schema->numAttr;
	int i;
	for(i=0;i<numAttrs;i++)
	{
		free(schema->attrNames[i]);
		schema->attrNames[i]=NULL;
	}
	free(schema->attrNames);
	free(schema->dataTypes);
	free(schema->keyAttrs);
	free(schema->typeLength);
	schema->attrNames=NULL;
	schema->dataTypes=NULL;
	schema->keyAttrs=NULL;
	schema->typeLength=NULL;
	free(schema);
	schema=NULL;
	return RC_OK;
}

char* mySerializeTableInfo(RM_TableData* rel)
{
	int tableInfoSize=TABLE_NAME+NUM_TUPLES;
	TableMgr* t=(TableMgr*)rel->mgmtData;
	char* tableInfo=(char*)calloc(tableInfoSize,sizeof(char));
	//serialize table name, 30 bytes
	memcpy(tableInfo,rel->name,strlen(rel->name));
	tableInfo+=TABLE_NAME;

	//serialize number of tuples, 5 bytes
	sprintf(tableInfo,"%i",getNumTuples(rel));
	tableInfo+=NUM_TUPLES;

	//append the result
	char* result=(char*)calloc(PAGE_SIZE,sizeof(char));//under the condition that one page is enough
	char* schemaInfo=mySerializeSchema(rel->schema);
	tableInfo-=tableInfoSize;

	memcpy(result,tableInfo,tableInfoSize);

	result+=tableInfoSize;
	memcpy(result,schemaInfo,t->schemaSize);

	result-=tableInfoSize;

	free(tableInfo);
	tableInfo=NULL;
	free(schemaInfo);
	schemaInfo=NULL;
	return result;
}

int getSchemaSize(int numAttr, int keySize)
{
	return NUM_ATTRS+NUM_KEYS+(ATTR_TYPE_LENGTH+ATTR_NAME)*numAttr+KEY_POSITION*keySize;
}
char* mySerializeSchema(Schema* schema)
{
	int schemaSize=getSchemaSize(schema->numAttr,schema->keySize);
	char* schemaInfo=(char*)calloc(schemaSize,sizeof(char));

	//serialize numAttrs, 5 bytes
	sprintf(schemaInfo,"%i",schema->numAttr);
	schemaInfo+=NUM_ATTRS;

	//serialize numKeys, 5 bytes
	sprintf(schemaInfo,"%i",schema->keySize);
	schemaInfo+=NUM_KEYS;

	//serialize attribute types, lengths 5 bytes each block, total numAttrs block
	int i;
	for(i=0;i<schema->numAttr;i++)
	{
		switch(schema->dataTypes[i])
		{
			case DT_INT: sprintf(schemaInfo,"%i",DT_INT);schemaInfo+=1;sprintf(schemaInfo,"%i",schema->typeLength[i]);schemaInfo+=ATTR_TYPE_LENGTH-1;break;
			case DT_STRING: sprintf(schemaInfo,"%i",DT_STRING);schemaInfo+=1;sprintf(schemaInfo,"%i",schema->typeLength[i]);schemaInfo+=ATTR_TYPE_LENGTH-1;break;
			case DT_FLOAT: sprintf(schemaInfo,"%i",DT_FLOAT);schemaInfo+=1;sprintf(schemaInfo,"%i",schema->typeLength[i]);schemaInfo+=ATTR_TYPE_LENGTH-1;break;
			case DT_BOOL: sprintf(schemaInfo,"%i",DT_BOOL);schemaInfo+=1;sprintf(schemaInfo,"%i",schema->typeLength[i]);schemaInfo+=ATTR_TYPE_LENGTH-1;break;
		}
	}

	//serialize attribute names, 20 bytes each, total numAttrs block
	for(i=0;i<schema->numAttr;i++)
	{
		strcpy(schemaInfo,schema->attrNames[i]);
		schemaInfo+=ATTR_NAME;
	}

	//serialize key attributes positions, 2 bytes each, total keySize block
	for(i=0;i<schema->keySize;i++)
	{
		sprintf(schemaInfo,"%i",schema->typeLength[i]);
		schemaInfo+=KEY_POSITION;
	}

	//append the result
	schemaInfo-=schemaSize;
	return schemaInfo;
}

void myDeSerializeSchema(Schema* schema, char* info)
{
	//deserialize numAttrs
	char* temp=(char*)calloc(NUM_ATTRS,sizeof(char));
	memcpy(temp,info,NUM_ATTRS);
	schema->numAttr=atoi(temp);
	info+=NUM_ATTRS;
	free(temp);
	temp=NULL;

	//deserialize numKeys
	temp=(char*)calloc(NUM_KEYS,sizeof(char));
	memcpy(temp,info,NUM_KEYS);
	schema->keySize=atoi(temp);
	info+=NUM_KEYS;
	free(temp);
	temp=NULL;

	//malloc space for attrs in schema
	int numAttr=schema->numAttr;
	schema->attrNames=(char**)malloc(sizeof(char*)*numAttr);
	schema->dataTypes=(DataType*)malloc(sizeof(DataType)*numAttr);
	schema->typeLength=(int*)malloc(sizeof(int)*numAttr);
	schema->keyAttrs=(int*)malloc(sizeof(int)*numAttr);

	//init attrNames
	int i;
	for(i=0;i<numAttr;i++)
	{
		schema->attrNames[i]=(char*)malloc(sizeof(char)*ATTR_NAME);//remember to free char** iteratively
	}

	//deserialize attrType, lengths
	temp=(char*)calloc(1,sizeof(char));
	char* temp1=(char*)calloc(ATTR_TYPE_LENGTH-1,sizeof(char));
	for(i=0;i<schema->numAttr;i++)
	{
		memcpy(temp,info,1);
		info+=1;
		memcpy(temp1,info,ATTR_TYPE_LENGTH-1);
		info+=ATTR_TYPE_LENGTH-1;
		schema->dataTypes[i]=atoi(temp);
		schema->typeLength[i]=atoi(temp1);
	}
	free(temp);
	temp=NULL;
	free(temp1);
	temp1=NULL;

	//deserialize attr names
	temp=(char*)calloc(ATTR_NAME,sizeof(char));
	for(i=0;i<schema->numAttr;i++)
	{
		memcpy(temp,info,ATTR_NAME);
		info+=ATTR_NAME;
		strcpy(schema->attrNames[i],temp);
	}
	free(temp);
	temp=NULL;

	//deserialize key positions
	temp=(char*)calloc(KEY_POSITION,sizeof(char));
	for(i=0;i<schema->keySize;i++)
	{
		memcpy(temp,info,KEY_POSITION);
		info+=KEY_POSITION;
		schema->keyAttrs[i]=atoi(temp);
	}
	free(temp);
	temp=NULL;
}

void myDeSerializeTableInfo(RM_TableData* rel, char* info)
{
	//deserialize table name
	rel->name=(char*)calloc(TABLE_NAME,sizeof(char));
	memcpy(rel->name,info,TABLE_NAME);
	info+=TABLE_NAME;

	//deserialize numTuples
	char* temp=(char*)calloc(NUM_TUPLES,sizeof(char));
	memcpy(temp,info,NUM_TUPLES);
	TableMgr* t=(TableMgr*)rel->mgmtData;
	t->numTuple=atoi(temp);
	free(temp);
	temp=NULL;
	info+=NUM_TUPLES;

	//deserialize schema
	rel->schema=(Schema*)malloc(sizeof(Schema));
	myDeSerializeSchema(rel->schema,info);
}

// table and manager
RC initRecordManager (void *mgmtData)
{
	return RC_OK;
}

RC shutdownRecordManager ()
{
	return RC_OK;
}

RC createTable (char *name, Schema *schema)
{
	//create a file named of table name
	CHECK(createPageFile(name));

	//open the buffer pool to r/w file
	BM_BufferPool *bm = MAKE_POOL();
	BM_PageHandle *h = MAKE_PAGE_HANDLE();
	CHECK(initBufferPool(bm, name, 3, RS_LRU, NULL));

	//store the table information in the first page
	int i=0;
	CHECK(pinPage(bm,h,i));//read the empty first page into memory
	RM_TableData rel;//init a relation to be serialized
	rel.name=name;
	rel.schema=schema;
	TableMgr t;//used to fetch number of tuples
	t.schemaSize=getSchemaSize(schema->numAttr,schema->keySize);
	t.numTuple=0;
	rel.mgmtData=&t;
	char* result=mySerializeTableInfo(&rel);//we use serialize to write table info
	memcpy(h->data,result,PAGE_SIZE);//write the result into first page of page file
	CHECK(markDirty(bm, h));
    CHECK(unpinPage(bm, h));
	CHECK(shutdownBufferPool(bm));//close the buffer pool
	free(result);
	result=NULL;
	return RC_OK;
}

RC openTable (RM_TableData *rel, char *name)
{
	//open the buffer pool to r/w file
	BM_BufferPool *bm = MAKE_POOL();
	BM_PageHandle *h = MAKE_PAGE_HANDLE();
	CHECK(initBufferPool(bm, name, 3, RS_LRU, NULL));

	//init Relation
	TableMgr* t=(TableMgr*)malloc(sizeof(TableMgr));
	rel->mgmtData=t;
	t->bm=bm;
	t->h=h;

	//restore the table information in the first page
	int i=0;
	CHECK(pinPage(bm,h,i));//read the first page containing the table information
	myDeSerializeTableInfo(rel,h->data);//input h.data into deserialize
	CHECK(unpinPage(bm, h));
	return RC_OK;
}

RC closeTable (RM_TableData *rel)
{
	//first shut down buffer pool
	TableMgr* t=(TableMgr*)rel->mgmtData;
	CHECK(shutdownBufferPool(t->bm));
	free(t->bm);
	free(t->h);
	free(rel->mgmtData);
	//then free memory allocated for table
	freeSchema(rel->schema);
	free(rel->name);
	t->bm=NULL;
	t->h=NULL;
	rel->mgmtData=NULL;
	rel->schema=NULL;
	rel->name=NULL;
	return RC_OK;
}

RC deleteTable (char *name)
{
	CHECK(destroyPageFile(name));
	return RC_OK;
}

int getNumTuples (RM_TableData *rel)
{
	TableMgr* t=(TableMgr*)rel->mgmtData;
	return t->numTuple;
}

// handling records in a table
RC insertRecord (RM_TableData *rel, Record *record)
{
	TableMgr* t=(TableMgr*)rel->mgmtData;
	BM_BufferPool *bm = t->bm;
	BM_PageHandle *h = t->h;
	PoolMgr * poolMgr = (PoolMgr *)bm->mgmtData;
	SM_FileHandle* fHandle=&poolMgr->fHandle;
	int i,j;
	int page, slot;
	int pageHeaderSize=getPageHeaderSize(rel);
	bool isFound=FALSE;
	//start searching the first empty slot for insertion. skip the first table info page
	//fHandle->curPagePos=1;
	for(i=1;i<fHandle->totalNumPages;i++)
	{
		fHandle->curPagePos=i;
		CHECK(pinPage(bm,h,fHandle->curPagePos));
		char* data=h->data;
		for(j=0;j<pageHeaderSize;j++)
		{
			if(data[j]=='0')
			{
				slot=j;
				isFound=TRUE;
				break;
			}
		}
		CHECK(unpinPage(bm, h));
		if(isFound)
		{
			page=i;
			break;
		}
	}
	if(isFound)
	{
		record->id.page=page;
		record->id.slot=slot;
		updateRecord(rel,record);
	}
	else
	{
		//increase the size of the file by appending one empty page to the end
		appendEmptyBlock(fHandle);
		//pin the newly appended page
		fHandle->curPagePos++;
		page=fHandle->curPagePos;
		slot=0;
		CHECK(pinPage(bm,h,page));
		//init the page header to be all 0
		for(i=0;i<pageHeaderSize;i++)
		{
			memcpy(h->data+i,"0",1);
		}
		CHECK(markDirty(bm, h));
		CHECK(unpinPage(bm,h));
		//updateRecord
		record->id.page=page;
		record->id.slot=slot;
		updateRecord(rel,record);
	}
	return RC_OK;
}

RC deleteRecord (RM_TableData *rel, RID id)
{
	TableMgr* t=(TableMgr*)rel->mgmtData;
	BM_BufferPool *bm = t->bm;
	BM_PageHandle *h = t->h;
	int page=id.page;
	int slot=id.slot;
	CHECK(pinPage(bm,h,page));
	char* data=h->data;
	data+=slot;
	memcpy(data,"0",1);//0 represents the record is deleted, as a tombstone
	data-=slot;
	CHECK(markDirty(bm, h));
	CHECK(unpinPage(bm, h));
	return RC_OK;
}

RC updateRecord (RM_TableData *rel, Record *record)
{
	TableMgr* t=(TableMgr*)rel->mgmtData;
	BM_BufferPool *bm = t->bm;
	BM_PageHandle *h = t->h;
	RID id=record->id;
	char* content=record->data;
	int page=id.page;
	int slot=id.slot;
	CHECK(pinPage(bm,h,page));
	char* data=h->data;
	int offset=0;
	int recordSize=getRecordSize(rel->schema);
	memcpy(data+slot,"1",1);//update the page header
	offset+=getPageHeaderSize(rel);//get over the page header area
	offset+=recordSize*slot;
	data+=offset;
	memcpy(data,content,recordSize);
	data-=offset;
	CHECK(markDirty(bm, h));
	CHECK(unpinPage(bm, h));
	return RC_OK;
}

RC getRecord (RM_TableData *rel, RID id, Record *record)
{
	TableMgr* t=(TableMgr*)rel->mgmtData;
	BM_BufferPool *bm = t->bm;
	BM_PageHandle *h = t->h;

	//fill up record.rid
	record->id.page=id.page;
	record->id.slot=id.slot;

	//fill up record.data
	int recordSize=getRecordSize(rel->schema);
	int page=id.page;
	int slot=id.slot;
	CHECK(pinPage(bm,h,page));
	char* data=h->data;
	int offset=0;
	offset+=getPageHeaderSize(rel);//get over the page header area
	offset+=recordSize*slot;
	data+=offset;
	memcpy(record->data,data,recordSize);
	data-=offset;
	CHECK(unpinPage(bm, h));
	return RC_OK;
}

// scans
RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond)
{
	scan->rel=rel;
	ScanMgr* scanMgr=(ScanMgr*)malloc(sizeof(ScanMgr));
	scanMgr->cond=cond;
	scanMgr->page=1;//init the first scan position to be page 1, slot 0
	scanMgr->slot=0;
	scan->mgmtData=scanMgr;
	return RC_OK;
}

RC next (RM_ScanHandle *scan, Record *record)
{
	//start searching from the first record
	ScanMgr* scanMgr=(ScanMgr*)scan->mgmtData;
	Expr* cond=scanMgr->cond;
	RM_TableData* rel=scan->rel;
	TableMgr* t=(TableMgr*)rel->mgmtData;
	BM_BufferPool *bm = t->bm;
	BM_PageHandle *h = t->h;
	PoolMgr * poolMgr = (PoolMgr *)bm->mgmtData;
	SM_FileHandle* fHandle=&poolMgr->fHandle;
	bool isFound=FALSE;
	int pageHeaderSize=getPageHeaderSize(rel);
	RID id;
	for(;scanMgr->page<fHandle->totalNumPages;scanMgr->page++)
	{
		fHandle->curPagePos=scanMgr->page;
		CHECK(pinPage(bm,h,fHandle->curPagePos));
		char* data=h->data;

		for(;scanMgr->slot<pageHeaderSize;scanMgr->slot++)
		{
			if(data[scanMgr->slot]=='1')
			{
				id.page=scanMgr->page;
				id.slot=scanMgr->slot;
				//getRecord
				CHECK(getRecord(rel,id,record));
				//resolve expressions
				Value* value;//this stores the bool value of the evaluation result of expression
				CHECK(evalExpr(record, rel->schema, cond, &value));//program stops here
				if(value->v.boolV)
				{
					isFound=TRUE;
					scanMgr->slot++;
					break;
				}
			}
		}
		CHECK(unpinPage(bm, h));
		if(isFound)
			break;
	}

	//if has next matched tuples
	if(isFound)
		return RC_OK;
	else
		return RC_RM_NO_MORE_TUPLES;
}

RC closeScan (RM_ScanHandle *scan)
{
	ScanMgr* scanMgr=(ScanMgr*)scan->mgmtData;
	Expr* cond=scanMgr->cond;
	CHECK(freeExpr(cond));
	free(scan->mgmtData);
	scan->mgmtData=NULL;
	return RC_OK;
}

// dealing with schemas
int getRecordSize (Schema *schema)
{
	int size=0,i;
	for(i=0;i<schema->numAttr;i++)
	{
		if(schema->typeLength[i]!=0)
			size+=schema->typeLength[i];
		else
			size+=sizeof(DT_INT);
	}
	return size;
}

Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys)
{
	//malloc for new schema
	Schema* schema=(Schema*)malloc(sizeof(Schema));
	schema->attrNames=attrNames;
	schema->dataTypes=dataTypes;
	schema->keyAttrs=keys;
	schema->keySize=keySize;
	schema->numAttr=numAttr;
	schema->typeLength=typeLength;
	return schema;
}

// dealing with records and attribute values
RC createRecord (Record **record, Schema *schema)
{
	(*record)=(Record*)malloc(sizeof(Record));
	(*record)->data=(char*)malloc(sizeof(char)*getRecordSize(schema));
	return RC_OK;
}

RC freeRecord (Record *record)
{
	free(record->data);
	record->data=NULL;
	return RC_OK;
}

RC getAttr (Record *record, Schema *schema, int attrNum, Value **value)
{
	*value=(Value*)malloc(sizeof(Value));
	int offset=0;
	char* result;
	char* data=record->data;
	int i;
	for(i=0;i<=attrNum;i++)
	{
		//get the position of data
		if(i!=attrNum)
		{
			if(schema->typeLength[i]!=0)
			{
				data+=schema->typeLength[i];
				offset+=schema->typeLength[i];
			}
			else
			{
				data+=sizeof(DT_INT);
				offset+=sizeof(DT_INT);
			}
		}
		else
			break;
	}
	//chunk the wanted attr values and copy to result
	(*value)->dt=schema->dataTypes[i];
	int attrSize=schema->typeLength[i]==0?sizeof(DT_INT):schema->typeLength[i];
	result=(char*)malloc(attrSize*sizeof(char)+1);
	memcpy(result,data,attrSize);
	result[attrSize]='\0';
	data-=offset;
	switch(schema->dataTypes[i])
	{
		case DT_INT:(*value)->v.intV=atoi(result);break;
		case DT_STRING:
			(*value)->v.stringV=(char*)malloc(strlen(result)*sizeof(char));
			strcpy((*value)->v.stringV,result);break;
		case DT_FLOAT:(*value)->v.floatV=atof(result);break;
		case DT_BOOL:(*value)->v.boolV=atoi(result);break;
	}
	free(result);
	result=NULL;
	return RC_OK;
}

RC setAttr (Record *record, Schema *schema, int attrNum, Value *value)
{
	char* result;
	switch(value->dt)
	{
		case DT_INT:
			result=(char*)calloc(sizeof(DT_INT), sizeof(char));
			sprintf(result,"%i",value->v.intV);
			break;
		case DT_STRING:
			result=(char*)calloc(strlen(value->v.stringV), sizeof(char));
			strcpy(result,value->v.stringV);
			break;
		case DT_FLOAT:
			result=(char*)calloc(sizeof(DT_FLOAT), sizeof(char));
			sprintf(result,"%f",value->v.floatV);
			break;
		case DT_BOOL:
			result=(char*)calloc(sizeof(DT_BOOL), sizeof(char));
			sprintf(result,"%i",value->v.boolV);
			break;
	}

	int i,offset=0;
	for(i=0;i<=attrNum;i++)
	{
		//find the position to modify
		if(i!=attrNum)
		{
			if(schema->typeLength[i]!=0)
			{
				record->data+=schema->typeLength[i];
				offset+=schema->typeLength[i];
			}
			else
			{
				record->data+=sizeof(DT_INT);
				offset+=sizeof(DT_INT);
			}
		}
		else
			break;
	}

	//chunk the wanted attr values and copy to result
	int attrSize=schema->typeLength[i]==0?sizeof(DT_INT):schema->typeLength[i];
	memcpy(record->data,result,attrSize);
	record->data-=offset;
	free(result);
	result=NULL;
	return RC_OK;
}
