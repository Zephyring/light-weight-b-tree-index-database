#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"
#include "rm_serializer.c"
#include "buffer_mgr.c"
#include "btree_mgr.h"

#define IDXID 30 //define the max length of idxid
#define ISLEAF 2 //if the node is a leaf

typedef struct TreeMgr
{
	int numNode;
	int numEntry;
	int n;
	int root;
	BM_BufferPool* bm;
	BM_PageHandle *h;
}TreeMgr;

typedef struct TreeScanMgr
{
    int node;// current node page number
    int entry;// current entry number
}TreeScanMgr;

typedef struct Node
{
	int id;//page number of this node
	int numKey;
	bool isLeaf;
	int next; //page number of next node
	int parent; //page number of parent node
	int* keys;
	RID* rids;
	int* children;
}Node;

//serialize and deserialize
char* mySerializeTreeInfo(BTreeHandle* btree);
void myDeserializeTreeInfo(BTreeHandle *bt, char* data);
char* mySerializeNode(Node* node);
void myDeserializeNode(Node* node, char* data);

//used for building tree
void initRoot(BTreeHandle* tree);
int findChild(Node* node, int target);
int findEntry(Node* node, int target);
int findBucket(BTreeHandle* tree, int target,bool* isExist);
Node* createNode(BTreeHandle* tree);
Node* loadNode(BTreeHandle* tree, int pageNum);
void saveNode(BTreeHandle* tree, Node* node);
void freeNode(Node* node);

//used for insertion
bool isFull(BTreeHandle* const tree, Node* const node);
void addEntry(Node* node, int target, RID rid);
void addChildren(Node* node, int target, Node* child);
void redistributeLeaf(Node* node, Node* newNode);
int redistributeNonLeaf(Node* node, Node* newNode);
void insertLeaf(BTreeHandle* tree, Node* node, int target, RID rid);
void insertNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target);
Node* splitLeaf(BTreeHandle* tree, Node* node, int target, RID rid);
Node* splitNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target, int* separator);

//used for deletion
void deleteLeaf(BTreeHandle* tree, Node* node, int target);
bool hasMoreThanHalf(BTreeHandle* tree, Node* node);
void removeEntry(Node* node, int target);
bool hasLeftSib(BTreeHandle* tree, Node* node);
bool hasRightSib(BTreeHandle* tree, Node* node);
void borrowFromLeafSib(BTreeHandle* tree, Node* sender, Node* receiver);
void mergeLeaf(BTreeHandle* tree, Node* mergedNode, Node* node, int target);
Node* loadLeftSib(BTreeHandle* tree, Node* node);
Node* loadRightSib(BTreeHandle* tree, Node* node);
void deleteNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target);
void removeChildren(Node* node, Node* child, int target);

char* mySerializeTreeInfo(BTreeHandle* btree)
{
	int offset=0;
	char* result=(char*)calloc(PAGE_SIZE, sizeof(char));

	//serialize idxId
	memcpy(result,btree->idxId,strlen(btree->idxId));
	result+=IDXID;
	offset+=IDXID;

	//serialize keyType
	memcpy(result,&btree->keyType,4);
	result+=4;
	offset+=4;

	//serialize info in the TreeMgr
	TreeMgr* tm=(TreeMgr*)btree->mgmtData;

	//serialize n
	memcpy(result,&tm->n,4);
	result+=4;
	offset+=4;

	//serialize num of entries
	memcpy(result,&tm->numEntry,4);
	result+=4;
	offset+=4;

	//serialize num of nodes
	memcpy(result,&tm->numNode,4);
	result+=4;
	offset+=4;

	//serialize page num where root stores
	memcpy(result,&tm->root,4);
	result+=4;
	offset+=4;

	result-=offset;

	return result;
}

void myDeserializeTreeInfo(BTreeHandle *bt, char* data)
{
	memcpy(bt->idxId,data,IDXID);
	data+=IDXID;

	memcpy(&bt->keyType,data,4);
	data+=4;

	TreeMgr* tm=(TreeMgr*)bt->mgmtData;
	memcpy(&tm->n,data,4);
	data+=4;

	memcpy(&tm->numEntry,data,4);
	data+=4;

	memcpy(&tm->numNode,data,4);
	data+=4;

	memcpy(&tm->root,data,4);
}

char* mySerializeNode(Node* node)
{
	int offset=0;
	char* result=(char*)calloc(PAGE_SIZE, sizeof(char));

	//serialize id
	memcpy(result,&node->id,4);
	result+=4;
	offset+=4;

	//serialize numKey
	memcpy(result,&node->numKey,4);
	result+=4;
	offset+=4;

	//serialize isLeaf
	memcpy(result,&node->isLeaf, ISLEAF);
	result+=ISLEAF;
	offset+=ISLEAF;

	//serialize next
	memcpy(result,&node->next,4);
	result+=4;
	offset+=4;

	//serialize parent
	memcpy(result,&node->parent,4);
	result+=4;
	offset+=4;

	//serialize keys
	int i;
	for(i=0;i<node->numKey;i++)
	{
		memcpy(result,&node->keys[i],4);
		result+=4;
		offset+=4;
	}

	//serialize entries
	if(node->isLeaf)
	{
		for(i=0;i<node->numKey;i++)
		{
			memcpy(result,&node->rids[i].page,4);
			result+=4;
			offset+=4;
			memcpy(result,&node->rids[i].slot,4);
			result+=4;
			offset+=4;
		}
	}
	else//serialize children
	{
		if(node->numKey!=0)
			for(i=0;i<node->numKey+1;i++)
			{
				memcpy(result,&node->children[i],4);
				result+=4;
				offset+=4;
			}
	}

	result-=offset;

	return result;
}

void myDeserializeNode(Node* node, char* data)
{
	memcpy(&node->id,data,4);
	data+=4;

	memcpy(&node->numKey,data,4);
	data+=4;

	memcpy(&node->isLeaf,data,ISLEAF);
	data+=ISLEAF;

	memcpy(&node->next,data,4);
	data+=4;

	memcpy(&node->parent,data,4);
	data+=4;

	int i;
	for(i=0;i<node->numKey;i++)
	{
		memcpy(&node->keys[i],data,4);
		data+=4;
	}

	if(node->isLeaf)
	{
		for(i=0;i<node->numKey;i++)
		{
			memcpy(&node->rids[i].page,data,4);
			data+=4;
			memcpy(&node->rids[i].slot,data,4);
			data+=4;
		}
	}
	else
	{
		if(node->numKey!=0)
			for(i=0;i<node->numKey+1;i++)
			{
				memcpy(&node->children[i],data,4);
				data+=4;
			}
	}
}

// init and shutdown index manager
RC initIndexManager (void *mgmtData)
{
    return RC_OK;
}

RC shutdownIndexManager ()
{
	return RC_OK;
}

// create, destroy, open, and close an btree index
RC createBtree (char *idxId, DataType keyType, int n)
{
	//create a file named of tree name
	CHECK(createPageFile(idxId));
    
	//open the buffer pool to r/w file
	BM_BufferPool *bm = MAKE_POOL();
	BM_PageHandle *h = MAKE_PAGE_HANDLE();
	CHECK(initBufferPool(bm, idxId, 3, RS_LRU, NULL));
    
	//store the tree information in the first page
	int i=0;
	CHECK(pinPage(bm,h,i));//read the empty first page into memory
    
    //init tree mgr
	TreeMgr *tm  = (TreeMgr*)malloc(sizeof(TreeMgr));
    tm->numEntry = 0;
    tm->numNode  = 0;
    tm->n	     = n;
	tm->root	 = 1;//store the first root in page 1
    //init tree handle
    BTreeHandle *bt = (BTreeHandle*)malloc(sizeof(BTreeHandle));
	bt->keyType 	= keyType;
	bt->idxId		= idxId;
    bt->mgmtData 	= tm;

	char* result=mySerializeTreeInfo(bt);//we use serialize to write tree info
    memcpy(h->data,result,PAGE_SIZE);//write the result into first page of page file
	
	CHECK(markDirty(bm, h));
    CHECK(unpinPage(bm, h));
	CHECK(shutdownBufferPool(bm));//close the buffer pool
	free(result);
	result=NULL;
	return RC_OK;
    
}

RC openBtree (BTreeHandle **tree, char *idxId)
{
	//open the buffer pool to r/w file
    BM_BufferPool *bm = MAKE_POOL();
	BM_PageHandle *h = MAKE_PAGE_HANDLE();
	CHECK(initBufferPool(bm, idxId, 3, RS_LRU, NULL));

	int i=0;
	CHECK(pinPage(bm,h,i));//read the first page into memory

	//malloc space for tree
	*tree = (BTreeHandle *)malloc(sizeof(BTreeHandle));
	TreeMgr *tm = (TreeMgr*)malloc(sizeof(TreeMgr));
	tm->h  = h;
	tm->bm = bm;
	(*tree)->mgmtData=tm;
	(*tree)->idxId=(char*)malloc(sizeof(char)*IDXID);

	//deserialize to fill up tree structure
	//store the tree information in the first page
	myDeserializeTreeInfo(*tree, h->data);
	CHECK(unpinPage(bm, h));
	initRoot(*tree);
	return RC_OK;
}

RC closeBtree (BTreeHandle *tree)
{
    //free every malloc space in tree
    TreeMgr *tm = (TreeMgr*)tree->mgmtData;
    CHECK(shutdownBufferPool(tm->bm));
	free(tm->h);
	free(tm->bm);
	free(tree->mgmtData);
	free(tree->idxId);
	free(tree);
	return RC_OK;
}

RC deleteBtree (char *idxId)
{
    //delete file according to idxId
	CHECK(destroyPageFile(idxId));
	return RC_OK;
}

// access information about a b-tree
RC getNumNodes (BTreeHandle *tree, int *result)
{
    //get tree.mgmtdata.numNode to result
	TreeMgr *tm = (TreeMgr*)tree->mgmtData;
	*result=tm->numNode;
	return RC_OK;
}

RC getNumEntries (BTreeHandle *tree, int *result)
{
    //get tree.mgmtdata.numEntry to result
	TreeMgr *tm = (TreeMgr*)tree->mgmtData;
	*result=tm->numEntry;
	return RC_OK;
}

RC getKeyType (BTreeHandle *tree, DataType *result)
{
    //get tree.keyType to result
	*result=tree->keyType;
	return RC_OK;
}

// index access
RC findKey (BTreeHandle *tree, Value *key, RID *result)
{
    //search key according to Value *key
    //solution: linearly search in each node starting from root,
	//get the children that contain the key, get the result

	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;

    bool isFound=FALSE;
    int currentNode=treeMgr->root;
    int target=0;
    if(key->dt==DT_INT)
    	target=key->v.intV;

    while(currentNode!=-1)
    {
    	Node* node=loadNode(tree,currentNode);
		int pos;
		if(!node->isLeaf)
		{
			pos=findChild(node,target);
			currentNode=node->children[pos];
		}
		else
		{
			pos=findEntry(node,target);
			if(pos!=-1)
			{
				result->page=node->rids[pos].page;
				result->slot=node->rids[pos].slot;
				isFound=TRUE;
				freeNode(node);
				break;
			}
			else
			{
				freeNode(node);
				break;
			}
		}
		freeNode(node);
    }

    if(isFound)
    	return RC_OK;
    else return RC_IM_KEY_NOT_FOUND;
}

int findChild(Node* node, int target)
{
	int i,pos=0;
	for(i=0;i<node->numKey;i++)
	{
		if(target>=node->keys[i])
			pos=i+1;
	}
	return pos;
}

int findEntry(Node* node, int target)
{
	//find the entry according to key
	//if is found, return the position of entry
	//else return -1
	int i,pos=-1;
	for(i=0;i<node->numKey;i++)
	{
		if(target==node->keys[i])
			pos=i;
	}
	return pos;
}

void initRoot(BTreeHandle* tree)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	if(treeMgr->numNode==0)
	{
		Node* root=createNode(tree);
		root->id=1;
		root->isLeaf=TRUE;
		saveNode(tree,root);
		freeNode(root);
		treeMgr->numNode++;
	}
}

RC insertKey (BTreeHandle *tree, Value *key, RID rid)
{
//  Perform a search to determine what bucket the new record should go into
//	If the bucket is not full (at most b - 1 entries after the insertion), add the record.
//	Otherwise, split the bucket.
//		Allocate new leaf and move half the bucket's elements to the new bucket.
//		Insert the new leaf's smallest key and address into the parent.
//		If the parent is full, split it too.
//			Add the middle key to the parent node.
//		Repeat until a parent is found that need not split.
//	If the root splits, create a new root which has one key and two pointers.

	int target=0;
	if(key->dt==DT_INT)
		target=key->v.intV;
	bool isExist=FALSE;
	int bucket=findBucket(tree,target,&isExist);

	if(isExist)
		return RC_IM_KEY_ALREADY_EXISTS;
	else
	{
		Node* node=loadNode(tree,bucket);
		insertLeaf(tree, node, target, rid);
		saveNode(tree,node);
		freeNode(node);
		return RC_OK;
	}
}

void insertLeaf(BTreeHandle* tree, Node* node, int target, RID rid)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	
    if(!isFull(tree, node))
    {
        addEntry(node,target,rid);
    }
    else
    {
        Node* newNode=splitLeaf(tree,node,target,rid);
        treeMgr->numNode++;//add node for the new splitted one
        if(node->parent!=-1)//has parent
        {
            Node* parent=loadNode(tree,node->parent);
            int target=newNode->keys[0];
            insertNonLeaf(tree,parent,newNode,target);
            saveNode(tree,parent);
            saveNode(tree,newNode);
            freeNode(parent);
            freeNode(newNode);
        }
        else
        {
            Node* root=createNode(tree);
            treeMgr->numNode++;//add node for the new root
            treeMgr->root=treeMgr->numNode;
            root->id=treeMgr->numNode;
            root->keys[0]=newNode->keys[0];
            root->numKey=1;
            root->children[0]=node->id;
            root->children[1]=newNode->id;
            node->parent=root->id;
            newNode->parent=root->id;
            saveNode(tree, root);
            saveNode(tree,newNode);
            freeNode(root);
            freeNode(newNode);
        }
    }
    treeMgr->numEntry++;
}

void insertNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target)
{
    TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
    
    if(!isFull(tree, node))
    {
        addChildren(node, target, child);
    }
    else
    {
        int separator=0;
        Node* newNode=splitNonLeaf(tree,node,child, target, &separator);
        treeMgr->numNode++;
        if(node->parent!=-1)
        {
        	Node* parent=loadNode(tree, node->parent);
            insertNonLeaf(tree, parent, newNode, target);
            addChildren(node,target, child);
            saveNode(tree,parent);
            saveNode(tree,newNode);
            freeNode(parent);
            freeNode(newNode);
        }
        else
        {
            Node* root=createNode(tree);
            treeMgr->numNode++;//add node for the new root
            treeMgr->root=treeMgr->numNode;
            root->id=treeMgr->numNode;
            root->keys[0]=newNode->keys[0];
            root->numKey=1;
            root->children[0]=node->id;
            root->children[1]=newNode->id;
            node->parent=root->id;
			newNode->parent=root->id;
			saveNode(tree, root);
			saveNode(tree,newNode);
            freeNode(root);
            freeNode(newNode);
        }
    }
}

Node* splitLeaf(BTreeHandle* tree, Node* node, int target, RID rid)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	Node* newNode=createNode(tree);
	newNode->id=treeMgr->numNode+1;
    newNode->keys[0]=target;
    newNode->rids[0]=rid;
    newNode->isLeaf = TRUE;
    redistributeLeaf(node,newNode);
	return newNode;
}

Node* splitNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target, int* separator)
{
    Node* newNode=createNode(tree);
    newNode->keys[0]=target;
    newNode->children[0]=child->id;
    int pos=findChild(node, target)+1;
    int i=node->numKey;
    for(;i>=pos;i--)
    {
        newNode->children[i-pos+1]=node->children[i];
        node->children[i]=-1;
    }
    *separator=redistributeNonLeaf(node, newNode);
    return newNode;
}

void redistributeLeaf(Node* node, Node* newNode)
{
	//move half the bucket's elements to the new bucket.
	int* key1=node->keys;
	int* key2=newNode->keys;
	int n=node->numKey+1;
	int i=0;
	int* keys = (int*)malloc(sizeof(int)*n);
	for (i = 0; i < n-1; i++)
	{
		keys[i] = key1[i];
	}
	keys[n-1] = key2[0];
	RID* rid1=node->rids;
	RID* rid2=newNode->rids;
	RID* rids = (RID*)malloc(sizeof(RID)*n);
	for (i = 0; i < n-1; i++)
	{
		rids[i]=rid1[i];
	}
	rids[n-1] = rid2[0];
	i = n-2;
	while (i >= 0)
	{
		if (keys[i] > keys[i+1])
		{
			int tempKey = keys[i];
			keys[i] = keys[i+1];
			keys[i+1] = tempKey;
			RID tempRid = rids[i];
			rids[i] = rids[i+1];
			rids[i+1] = tempRid;
			i--;
		} else
		{
			break;
		}
	}
	int middle=0;
	if(n%2)
		middle=n/2+1;
	else middle=n/2;
	for(i=0;i<n;i++)
	{
		if(i<middle)
		{
			node->keys[i]=keys[i];
			node->rids[i]=rids[i];
		}
		else
		{
			newNode->keys[i-middle]=keys[i];
			newNode->rids[i-middle]=rids[i];
		}
	}
	newNode->next=node->next;
	node->next=newNode->id;
	node->numKey=middle;
	newNode->numKey=n-middle;
	free(keys);
	free(rids);
}

int redistributeNonLeaf(Node* node, Node* newNode)
{
    int* key1=node->keys;
    int* key2=newNode->keys;
    int n=node->numKey+1;
    int i=0;
    int* keys = (int*)malloc(sizeof(int)*n);
    for (i = 0; i < n-1; i++)
    {
        keys[i] = key1[i];
    }
    keys[n] = key2[0];
    i = n-1;
    while (i >= 0)
    {
        if (keys[i] > keys[i+1])
        {
            int tempKey = keys[i];
            keys[i] = keys[i+1];
            keys[i+1] = tempKey;
            i--;
        } else
        {
            break;
        }
    }
    int middle=n/2;
    int separator=0;
    for(i=0;i<n;i++)
    {
        if(i<middle)
        {
            node->keys[i]=keys[i];
        }
        else if(i==middle)
        {
            separator=keys[middle];
        }
        else
        {
            newNode->keys[i-middle]=keys[i];
        }
    }
    free(keys);
    return separator;
}

void addChildren(Node* node, int target, Node* child)
{
	//Insert the new leaf's smallest key and address into the parent.
    int pos=findChild(node, target);
    int i;
    //insert key and child
    for(i=node->numKey-1;i>=pos;i--)
    {
        node->keys[i+1]=node->keys[i];
        node->children[i+1]=node->children[i];
    }
    node->keys[pos]=target;
    node->children[pos+1]=child->id;
    node->numKey++;
}

void addEntry(Node* node, int target, RID rid)
{
	int pos=findChild(node,target);
	int i;
	//insert key and entry
	for(i=node->numKey-1;i>=pos;i--)
	{
		node->keys[i+1]=node->keys[i];
		node->rids[i+1]=node->rids[i];
	}
	node->keys[pos]=target;
	node->rids[pos]=rid;
	node->numKey++;
}

bool isFull(BTreeHandle* const tree, Node* const node)
{
	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	if(node->numKey==treeMgr->n)
		return TRUE;
	else return FALSE;
}

Node* createNode(BTreeHandle* tree)
{
	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	Node* node=(Node*)malloc(sizeof(Node));
	node->keys=(int*)malloc(sizeof(int)*treeMgr->n);
	node->rids=(RID*)malloc(sizeof(RID)*treeMgr->n);
	node->children=(int*)malloc(sizeof(int)*(treeMgr->n+1));
	node->id=-1;
	node->isLeaf=FALSE;
	node->next=-1;
	node->numKey=0;
	node->parent=-1;
	return node;
}
Node* loadNode(BTreeHandle* tree, int pageNum)
{
	//load the disk page node into memory node
	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	BM_BufferPool *bm = treeMgr->bm;
	BM_PageHandle *h = treeMgr->h;

	Node* node=createNode(tree);

	CHECK(pinPage(bm,h,pageNum));
	myDeserializeNode(node,h->data);
	CHECK(unpinPage(bm, h));

	return node;
}

void saveNode(BTreeHandle* tree, Node* node)
{
	//save the memory node into disk
	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	BM_BufferPool *bm = treeMgr->bm;
	BM_PageHandle *h = treeMgr->h;
	PoolMgr *poolMgr = (PoolMgr*)bm->mgmtData;
	SM_FileHandle* fHandle = &poolMgr->fHandle;

	fHandle->curPagePos=node->id;
	if(fHandle->totalNumPages<=fHandle->curPagePos)
		appendEmptyBlock(fHandle);

	CHECK(pinPage(bm,h,node->id));
	h->data=mySerializeNode(node);
	CHECK(markDirty(bm, h));
	CHECK(unpinPage(bm, h));
}

void freeNode(Node* node)
{
	free(node->keys);
	free(node->rids);
	free(node->children);
	free(node);
}

int findBucket(BTreeHandle* tree, int target, bool* isExist)
{
	//find the bucket that target should be inserted into
	//if key is already in the tree, return isExist =TRUE
	//return the bucket page number

	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	int currentNode=treeMgr->root;
	int bucket=0;

	while(currentNode!=-1)
	{
		Node* node=loadNode(tree,currentNode);
		int pos;
		if(!node->isLeaf)
		{
			pos=findChild(node,target);
			currentNode=node->children[pos];
		}
		else
		{
			pos=findEntry(node,target);
			bucket=node->id;
			if(pos!=-1)
				*isExist=TRUE;
			freeNode(node);
			break;
		}

	}

	return bucket;
}

bool hasMoreThanHalf(BTreeHandle* tree, Node* node)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	if(node->numKey>treeMgr->n/2)
		return TRUE;
	else return FALSE;
}

void removeEntry(Node* node, int target)
{
	int pos=findEntry(node,target);
	if(pos!=-1)
	{
		if(pos!=node->numKey-1)
		{
			int i;
			for(i=pos;i<node->numKey-1;i++)
			{
				node->keys[i]=node->keys[i+1];
				node->rids[i]=node->rids[i+1];
			}
		}
		node->numKey--;
	}
}

bool hasLeftSib(BTreeHandle* tree, Node* node)
{
	if(node->parent==-1)
		return FALSE;
	Node* parent=loadNode(tree,node->parent);
	int pos=findChild(parent,node->id);
	freeNode(parent);
	if(pos!=0)
		return TRUE;
	else return FALSE;
}

bool hasRightSib(BTreeHandle* tree, Node* node)
{
	if(node->parent==-1)
		return FALSE;
	Node* parent=loadNode(tree,node->parent);
	int pos=findChild(parent,node->id);
	int n=parent->numKey;
	freeNode(parent);
	if(pos!=n)
		return TRUE;
	else return FALSE;
}

void borrowFromLeafSib(BTreeHandle* tree, Node* sender, Node* receiver)
{
	int temp=sender->keys[0];
	int temp1=receiver->keys[0];
	Node* parent=loadNode(tree,sender->parent);
	int senderPos=findChild(parent,sender->keys[0]);
	if(temp<temp1)//borrow from left sib
	{
		int key=sender->keys[sender->numKey-1];//borrow the max key from left sib
		RID rid=sender->rids[sender->numKey-1];
		removeEntry(sender,key);
		addEntry(receiver,key,rid);
		parent->keys[senderPos]=receiver->keys[0];
	}
	else//borrow from right sib
	{
		int key=sender->keys[0];//borrow the min key from right sib
		RID rid=sender->rids[0];
		removeEntry(sender,key);
		addEntry(receiver,key,rid);
		parent->keys[senderPos-1]=sender->keys[0];
	}
	saveNode(tree,parent);
	freeNode(parent);
}

void mergeLeaf(BTreeHandle* tree, Node* mergedNode, Node* node, int target)
{
	int i;
	Node* parent=loadNode(tree,node->parent);
	for(i=0;i<node->numKey;i++)
	{
		addEntry(mergedNode,node->keys[i],node->rids[i]);
	}
	deleteNonLeaf(tree, parent,mergedNode,target);
	saveNode(tree,parent);
	freeNode(parent);
}

void deleteNonLeaf(BTreeHandle* tree, Node* node, Node* child, int target)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	if(hasMoreThanHalf(tree,node))
		removeChildren(node,child,target);
	else
	{
		treeMgr->root=child->id;
		child->next=-1;
		child->parent=-1;
		saveNode(tree,child);
		freeNode(child);
	}
}

void removeChildren(Node* node, Node* child, int target)
{
	int pos1=findChild(node,target);
	int pos2=findChild(node,child->keys[0]);
	int i;
	if(pos1>pos2)//merge left
	{
		for(i=pos1;i<node->numKey;i++)//move keys, children leftwards
		{
			node->keys[i-1]=node->keys[i];
			node->children[i]=node->children[i+1];
		}
	}
	else//merge right
	{
		for(i=pos1;i<node->numKey-1;i++)
		{
			node->keys[i]=node->keys[i+1];
		}
		for(i=pos1;i<node->numKey;i++)//move keys, children leftwards
		{
			node->children[i]=node->children[i+1];
		}
	}
	node->numKey--;
}

Node* loadLeftSib(BTreeHandle* tree, Node* node)
{
	Node* parent=loadNode(tree,node->parent);
	int left=parent->children[findChild(parent,node->id)-1];
	Node* leftSib=loadNode(tree,left);
	freeNode(parent);
	return leftSib;
}

Node* loadRightSib(BTreeHandle* tree, Node* node)
{
	Node* parent=loadNode(tree,node->parent);
	int right=parent->children[findChild(parent,node->id)+1];
	Node* rightSib=loadNode(tree,right);
	freeNode(parent);
	return rightSib;
}

void deleteLeaf(BTreeHandle* tree, Node* node, int target)
{
	TreeMgr* treeMgr=(TreeMgr*)tree->mgmtData;
	removeEntry(node,target);
	if(!hasMoreThanHalf(tree,node))
	{
		if(hasLeftSib(tree,node)&&hasRightSib(tree,node))
		{
			Node* leftSib=loadLeftSib(tree,node);
			Node* rightSib=loadRightSib(tree,node);
			if(hasMoreThanHalf(tree,leftSib))
			{
				borrowFromLeafSib(tree,leftSib,node);
			}
			else if(hasMoreThanHalf(tree, rightSib))
			{
				borrowFromLeafSib(tree,rightSib,node);
			}
			else
			{
				mergeLeaf(tree,leftSib,node,target);
			}
			saveNode(tree,leftSib);
			saveNode(tree,rightSib);
			freeNode(leftSib);
			freeNode(rightSib);
		}
		else if(!hasLeftSib(tree,node)&&hasRightSib(tree,node))
		{
			Node* rightSib=loadRightSib(tree,node);
			if(hasMoreThanHalf(tree,rightSib))
			{
				borrowFromLeafSib(tree,rightSib,node);
			}
			else
			{
				mergeLeaf(tree,rightSib,node,target);
			}
			saveNode(tree,rightSib);
			freeNode(rightSib);
		}
		else if(hasLeftSib(tree,node)&&!hasRightSib(tree,node))
		{
			Node* leftSib=loadLeftSib(tree,node);
			if(hasMoreThanHalf(tree,leftSib))
			{
				borrowFromLeafSib(tree,leftSib,node);
			}
			else
			{
				mergeLeaf(tree,leftSib,node,target);
			}
			saveNode(tree,leftSib);
			freeNode(leftSib);
		}
	}
	treeMgr->numEntry--;
}

RC deleteKey (BTreeHandle *tree, Value *key)
{
//	Start at root, find leaf L where entry belongs.
//	Remove the entry.
//	If L is at least half-full, done!
//	If L has fewer entries than it should,
//	Try to re-distribute, borrowing from sibling (adjacent node with same parent as L).
//	If re-distribution fails, merge L and sibling.
//	If merge occurred, must delete entry (pointing to L or sibling) from parent of L.
//	Merge could propagate to root, decreasing height.
	int target=0;
	if(key->dt==DT_INT)
		target=key->v.intV;
	bool isExist=FALSE;
	int bucket=findBucket(tree,target,&isExist);
	if(isExist)
	{
		Node* node=loadNode(tree,bucket);
		deleteLeaf(tree,node,target);
		saveNode(tree,node);
		freeNode(node);
		return RC_OK;
	}
	else
		return RC_IM_KEY_NOT_FOUND;
}

RC openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle)
{
    TreeScanMgr* treeScanMgr = (TreeScanMgr*)malloc(sizeof(TreeScanMgr));
    treeScanMgr->node=1;//page 1 stores the first leaf node
    treeScanMgr->entry=0;//start from entry 0 in the first node
    
    BT_ScanHandle *bHandle = (BT_ScanHandle*)malloc(sizeof(BT_ScanHandle));
    bHandle->tree = tree;
    bHandle->mgmtData = treeScanMgr;
    *handle = bHandle;    
    return RC_OK;
}

RC nextEntry (BT_ScanHandle *handle, RID *result)
{
    TreeScanMgr* treeScanMgr = (TreeScanMgr*)handle->mgmtData;
    
    bool isFound = FALSE;
    int currentNode=treeScanMgr->node;
    int currentEntry=treeScanMgr->entry;

    if(currentNode!=-1)
    {
    	Node* node=loadNode(handle->tree,currentNode);
    	result->page=node->rids[currentEntry].page;
    	result->slot=node->rids[currentEntry].slot;
    	isFound=TRUE;
    	if(currentEntry!=node->numKey-1)//if this is not the last entry of a node
    		treeScanMgr->entry++;
    	else//if this is the last entry of a node
    	{
    		treeScanMgr->node=node->next;
    		treeScanMgr->entry=0;
    	}
        freeNode(node);
    }
    
    if (isFound)
        return RC_OK;
    else
        return RC_IM_NO_MORE_ENTRIES;
}

RC closeTreeScan (BT_ScanHandle *handle)
{
    free(handle->mgmtData);
    return RC_OK;
}

// debug and test functions
char *printTree (BTreeHandle *tree)
{
	TreeMgr* treeMgr = (TreeMgr*)tree->mgmtData;
	Node* node=loadNode(tree,treeMgr->root);
	int pos=0,i;
	char* result=(char*)calloc(PAGE_SIZE,sizeof(char));
	while(1)
	{
		if(!node->isLeaf)
		{
			printf("(%d)[",pos);
			for(i=0;i<node->numKey;i++)
			{
				printf("%d,%d,",pos+1+i,node->keys[i]);
			}
			printf("%d]\n",pos+1+i);
			int next=node->children[0];
			freeNode(node);
			node=loadNode(tree,next);
		}
		else
		{
			while(node->id!=-1)
			{
				printf("(%d)[",++pos);
				for(i=0;i<node->numKey;i++)
				{
					printf("%d.%d,%d,",node->rids[i].page,node->rids[i].slot,node->keys[i]);
				}
				if(node->next!=-1)
					printf("%d]\n",pos+1);
				else
				{
					printf("]\n");
					freeNode(node);
					return "";
				}
				int next=node->next;
				freeNode(node);
				node=loadNode(tree,next);
			}
		}
	}

	return result;
}
