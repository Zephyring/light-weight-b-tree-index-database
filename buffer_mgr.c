#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

#include"buffer_mgr.h"
#include"storage_mgr.h"

typedef struct Frame
{
    bool isDirty;
    int fixCount;
    int replFlag;//time stamp of this frame
    BM_PageHandle page;
    struct Frame* next;
} Frame;

typedef struct PoolManager
{
	SM_FileHandle fHandle;
	int numRead;
	int numWrite;
	int maxFlag;//store the maximum of time stamp of frames list
	PageNumber *frameContent;
    bool *dirty;
	int *fixCount;
	Frame* head;
}PoolMgr;

void initFrame(Frame* frame);
void fillFrame(Frame* frame, int fixCount, bool isDirty, int pageNum, char* data);
void insertFrame(BM_BufferPool *const bm, Frame* frame);
int getMaxReplFlag(BM_BufferPool *const bm);
Frame* getMinFlagFrame(BM_BufferPool *const bm);

//initialize a frame with default value
void initFrame(Frame* frame)
{
	frame->fixCount=0;
	frame->isDirty=FALSE;
	frame->replFlag = -1;
	frame->next=NULL;
	frame->page.pageNum=-1;
	frame->page.data = (char *)malloc(sizeof(char)*PAGE_SIZE);
	memset(frame->page.data, 0, sizeof(char)*PAGE_SIZE);
}

//initialize a frame with parameters
void fillFrame(Frame* frame, int fixCount, bool isDirty, int pageNum, char* data)
{
	frame->fixCount=fixCount;
	frame->isDirty=isDirty;
	frame->page.pageNum=pageNum;
	frame->page.data=data;
}

//insert a frame to the head of the frames in the buffer pool
void insertFrame(BM_BufferPool *const bm, Frame* frame)
 {
	 if(frame==NULL)
		 printf("Frame is NULL");
	 else if(bm==NULL)
		 printf("BufferPool is NULL");
	 else
	 {
		 Frame* head=((PoolMgr*)bm->mgmtData)->head;
		 if(head!=NULL)
		 {
			 ((PoolMgr *)bm->mgmtData)->head = frame;
			 frame->next = head;
		 }
		 else
		 {
			 ((PoolMgr*)bm->mgmtData)->head=frame;
		 }
	 }
 }


RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
		  const int numPages, ReplacementStrategy strategy,
		  void *stratData)
{
	bm->pageFile=(char*)pageFileName;
	bm->numPages=numPages;
	bm->strategy=strategy;
	//create memory for poolMgr, since it should live as long as buffer pool
	//remember to free it in close buffer pool
	PoolMgr* poolMgr=(PoolMgr*)malloc(sizeof(PoolMgr));
	bm->mgmtData=poolMgr;
	poolMgr->numRead=0;
	poolMgr->numWrite=0;
	poolMgr->maxFlag=-1;

	//open page file and initialize fHandle
	openPageFile(bm->pageFile,&poolMgr->fHandle);

	//init the linked list to be of size numPages
	int i;
	Frame* frame;
	for(i=0;i<numPages;i++)
	{
		frame=(Frame*)malloc(sizeof(Frame));
		initFrame(frame);
		insertFrame(bm,frame);
	}

	//init static information
	poolMgr->frameContent=(PageNumber*)malloc(sizeof(PageNumber)*numPages);
	poolMgr->dirty=(bool*)malloc(sizeof(bool)*numPages);
	poolMgr->fixCount=(int*)malloc(sizeof(int)*numPages);

	return RC_OK;
}

RC shutdownBufferPool(BM_BufferPool *const bm)
{
    PoolMgr* poolMgr=(PoolMgr*)bm->mgmtData;
	Frame* frames=(Frame*)poolMgr->head;
    
    //always remember to free memory for frames
	int i = 0;
    while(i < bm->numPages)
    {
    	if(frames->fixCount!=0)
    	{
    		RC error=RC_WRITE_FAILED;
    		printError(error);
    		return error;
    	}
    	else if(frames->isDirty)
    	{
    		forceFlushPool(bm);
    	}
		frames=frames->next;
		i++;
    }
    //always remember to free memory for frames
    frames=(Frame*)poolMgr->head;
    i=0;
    while(i<bm->numPages)
    {
    	free(frames->page.data);
    	free(frames);
    	frames=frames->next;
    	i++;
    }
    //always remember to free static information
    free(poolMgr->frameContent);
    free(poolMgr->dirty);
    free(poolMgr->fixCount);
    //always remember to free memory for poolMgr
    free(poolMgr);
    poolMgr=NULL;
    return RC_OK;
}

RC forceFlushPool(BM_BufferPool *const bm)
{
	if(bm == NULL)
	{
		printf("Buffer Pool is NULL\n");
		return RC_WRITE_FAILED;
	}

    PoolMgr* poolMgr = (PoolMgr*)bm->mgmtData;
	Frame* frames = poolMgr->head;

	int i = 0;
	while (i < bm->numPages)
	{
		if (frames->isDirty && frames->fixCount == 0)
		{
			writeBlock(frames->page.pageNum, &poolMgr->fHandle, (SM_PageHandle)frames->page.data);
			frames->isDirty = FALSE;
		}
		frames=frames->next;
		i++;
	}
	return RC_OK;

}

// Buffer Manager Interface Access Pages
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page)
{
	if(bm==NULL)
		return RC_WRITE_FAILED;
	else
	{
		Frame* frames=((PoolMgr*)bm->mgmtData)->head;
		int pageNum=page->pageNum;
		while(frames!=NULL)
		{
			if(frames->page.pageNum==pageNum)
			{
				frames->isDirty=TRUE;
				break;
			}
			frames=frames->next;
		}
		return RC_OK;
	}
}

RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
	if(bm==NULL)
		return RC_WRITE_FAILED;
	else
	{
		int pageNum=page->pageNum;
		Frame* frames=((PoolMgr*)bm->mgmtData)->head;
		while(frames!=NULL)
		{
			if(frames->page.pageNum==pageNum)
			{
				frames->fixCount--;
				if (frames->isDirty) {
					frames->page.data=page->data;//update frame in the memory
					forcePage(bm, page);
				}
				//handle errors of over-unpinning
				if(frames->fixCount<0)
					return RC_WRITE_FAILED;
				break;
			}
			frames=frames->next;
		}
		return RC_OK;
	}
}

RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
	if(bm == NULL)
	{
		printf("Buffer Pool is NULL\n");
		return RC_WRITE_FAILED;
	}

    PoolMgr* pool = (PoolMgr*)bm->mgmtData;

	writeBlock(page->pageNum, &pool->fHandle, (SM_PageHandle)page->data);
    pool->numWrite++;
	return RC_OK;
}

int getMaxReplFlag(BM_BufferPool *const bm) {

	return ((PoolMgr*)bm->mgmtData)->maxFlag;
}

Frame* getMinFlagFrame(BM_BufferPool *const bm)
{
	PoolMgr* poolMgr=(PoolMgr*)bm->mgmtData;
	Frame* frame=poolMgr->head;
	int i,minFlag=INT_MAX;
	Frame* minFrame;
	for(i=0;i<bm->numPages;i++)
	{
		if(frame->replFlag<minFlag&&frame->fixCount==0)
		{
			minFlag=frame->replFlag;
			minFrame=frame;
		}
		frame=frame->next;
	}
	return minFrame;
}

RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
	    const PageNumber pageNum)
{
	PoolMgr * poolMgr = (PoolMgr *)bm->mgmtData;
	Frame * frame = poolMgr->head;
	int num = 0;

	//if the requested page is already in the pool, just simply return the page
	while (num < bm->numPages) {
		if (frame->page.pageNum == pageNum) {
			page->pageNum = pageNum;
			page->data 	  = frame->page.data;
			frame->fixCount++;
			//if LRU algorithm is applied, the time stamp should plus 1
			if (bm->strategy == RS_LRU) {
				frame->replFlag = getMaxReplFlag(bm) + 1;
				poolMgr->maxFlag=frame->replFlag;
			}
			return RC_OK;
		}
		frame = frame->next;
		num++;
	}

	//replace the first frame with smallest time stamp if fixCount !=0
	frame=getMinFlagFrame(bm);
	readBlock(pageNum,&poolMgr->fHandle,(SM_PageHandle)frame->page.data);
	poolMgr->numRead++;
	frame->fixCount++;
	frame->replFlag = getMaxReplFlag(bm) + 1;
	poolMgr->maxFlag=frame->replFlag;
	frame->page.pageNum = pageNum;
	page->pageNum=pageNum;
	page->data=frame->page.data;
	return RC_OK;
}

// Statistics Interface
PageNumber *getFrameContents (BM_BufferPool *const bm)
{
	if(bm==NULL)
	{
		printf("Buffer Pool is NULL\n");
		return NULL;
	}
	else
	{
		Frame* head=((PoolMgr*)bm->mgmtData)->head;
		int numPages=bm->numPages;
		PageNumber* content=((PoolMgr*)bm->mgmtData)->frameContent;
		if(content!=NULL)
		{
			int i;
			for(i=0;i<numPages;i++)
			{
				content[i]=head->page.pageNum;
				head=head->next;
			}
		}
		return  content;
	}
}

bool *getDirtyFlags (BM_BufferPool *const bm)
{
	if(bm==NULL)
	{
		printf("Buffer Pool NULL\n");
		return NULL;
	}
	else
	{
		Frame* head=((PoolMgr*)bm->mgmtData)->head;
		int numPages=bm->numPages;
		bool* dirtys=((PoolMgr*)bm->mgmtData)->dirty;
		if(dirtys!=NULL)
		{
			int i;
			for(i=0;i<numPages;i++)
			{
				dirtys[i]=head->isDirty;
				head=head->next;
			}
		}
		return dirtys;
	}
}

int *getFixCounts (BM_BufferPool *const bm)
{
	if(bm==NULL)
	{
		printf("Buffer Pool NULL\n");
		return NULL;
	}
	else
	{
		Frame* head=((PoolMgr*)bm->mgmtData)->head;
		int numPages=bm->numPages;
		int* fixCs=((PoolMgr*)bm->mgmtData)->fixCount;
		if(fixCs!=NULL)
		{
			int i;
			for(i=0;i<numPages;i++)
			{
				fixCs[i]=head->fixCount;
				head=head->next;
			}
		}
		return fixCs;
	}
}

int getNumReadIO (BM_BufferPool *const bm)
{
	return ((PoolMgr*)bm->mgmtData)->numRead;
}

int getNumWriteIO (BM_BufferPool *const bm)
{
	return ((PoolMgr*)bm->mgmtData)->numWrite;
}
