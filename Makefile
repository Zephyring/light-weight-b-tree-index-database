all: btree_mgr.c buffer_mgr.c buffer_mgr_stat.c dberror.c expr.c record_mgr.c rm_serializer.c storage_mgr.c test_assign4_1.c
	gcc -Xlinker -zmuldefs -o test_demo btree_mgr.c buffer_mgr.c buffer_mgr_stat.c dberror.c expr.c record_mgr.c rm_serializer.c storage_mgr.c test_assign4_1.c
	make expr

expr:	btree_mgr.c buffer_mgr.c buffer_mgr_stat.c dberror.c expr.c record_mgr.c rm_serializer.c storage_mgr.c test_expr.c
	gcc -Xlinker -zmuldefs -o test_expr buffer_mgr.c buffer_mgr_stat.c dberror.c expr.c record_mgr.c rm_serializer.c storage_mgr.c test_expr.c

clean:
	rm test_demo
	rm test_expr
